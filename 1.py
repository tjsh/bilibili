headers = {
"user-agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36",
"cookie": "_uuid=AD332E56-E088-8B2B-D9D1-D38D788E8D7795956infoc; buvid3=517F97C4-EB37-4ACC-937C-F488C44280C134769infoc; CURRENT_FNVAL=80; blackside_state=1; rpdid=|(Rlll|Jm)l0J'uYkkk)kR|~; PVID=1; sid=7bk16dol; buvid_fp=517F97C4-EB37-4ACC-937C-F488C44280C134769infoc; buvid_fp_plain=517F97C4-EB37-4ACC-937C-F488C44280C134769infoc; bsource=search_baidu; fingerprint=09f0934c6e7e340622427c44044f1c7d; DedeUserID=21275793; DedeUserID__ckMd5=1b4589fe5411a2d2; SESSDATA=1c9cfca2%2C1641532340%2C78dae*71; bili_jct=0507408220690bf828031b5e31690b5b",
"referer": "https://message.bilibili.com/"
}
import requests
import re
import json
import subprocess
import os
def send_request(url=0, p=1, i=0):
    response = requests.get(url=url, headers=headers)
    try:
        return response
    except TimeoutError:
        print('error!!!')
def get_video_data(html_data):
    title = re.findall('<title data-vue-meta="true">(.*?)</title>', html_data)[0].replace("_哔哩哔哩 (゜-゜)つロ 干杯~-bilibili", "")
    json_data = re.findall(r'<script>window.__playinfo__=(.*?)</script>', html_data)[0]
    json_data = json.loads(json_data)
    audio_url = json_data["data"]["dash"]["audio"][0]["backupUrl"][0]
    video_url = json_data["data"]["dash"]["video"][0]["backupUrl"][0]
    video_data = [title, audio_url, video_url]
    return video_data

def save_data(file_name, audio_url, video_url):
    print("正在下载 " + file_name + "的视频...")
    video_data = send_request(url=video_url).content
    audio = send_request(url=audio_url).content
    print(audio)
    print("完成下载 " + file_name + "的视频！")
    print("保存中......")
    with open("pp" + ".mp4", "wb") as f:
        f.write(video_data)
    with open("pp" + ".mp3", "wb") as f:
        f.write(audio)
    subprocess.call("ffmpeg -i pp.mp4 -i pp.mp3 -c:v copy -c:a aac -strict experimental oo.mp4", shell=True)
    os.rename("oo.mp4", file_name + ".mp4")
    os.remove("pp.mp4")
    os.remove("pp.mp3")
if __name__ == "__main__":
        url = input('bilibilili链接:\n')
        j = send_request(url=url).text
        q = get_video_data(j)
        save_data(q[0], q[1], q[2])